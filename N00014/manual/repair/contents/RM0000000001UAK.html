<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM19Y0E</span>
<span class="globalServcat">_V1</span>
<span class="globalServcatName">General</span>
<span class="globalSection">_002963</span>
<span class="globalSectionName">INTRODUCTION</span>
<span class="globalTitle">_0014845</span>
<span class="globalTitleName">HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS</span>
<span class="globalCategory">F</span>
</div>
<h1>INTRODUCTION&nbsp;&nbsp;HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS&nbsp;&nbsp;GENERAL INFORMATION&nbsp;&nbsp;</h1>
<br>
<div id="RM0000000001UAK_z0" class="category no00">
<div class="content5">
<div class="list1">
<div class="list1Item">
<div class="list1Head"></div>
<div class="list1Body"><p>A large number of ECU controlled systems are used in this vehicle. In general, ECU controlled systems are considered to be very intricate, requiring a high level of technical knowledge to troubleshoot. However, most problem checking procedures only involve inspecting the ECU controlled system's circuits one by one. An adequate understanding of the system and a basic knowledge of electricity is enough to perform effective troubleshooting, accurate diagnoses and necessary repairs.
</p>
</div>
</div>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">TROUBLESHOOTING PROCEDURES
</span></p>
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The troubleshooting procedures consist of diagnosis procedures for when a DTC is stored and diagnosis procedures for when no DTC is stored. The basic idea is explained in the following table.
</p>
<table summary="">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Procedure Type
</th>
<th class="alcenter">Details
</th>
<th class="alcenter">Troubleshooting Method
</th>
</tr>
</thead>
<tbody>
<tr>
<td>DTC Based Diagnosis
</td>
<td>The diagnosis procedure is based on the DTC that is stored.
</td>
<td>The malfunctioning part is identified based on the DTC detection conditions using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the intelligent tester and inspection of related parts.
</td>
</tr>
<tr>
<td>Symptom Based Diagnosis
<br>
(No DTCs stored)
</td>
<td>The diagnosis procedure is based on problem symptoms.
</td>
<td>The malfunctioning part is identified based on the problem symptoms using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the intelligent tester and inspection of related parts.
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Vehicle systems are complex and use many ECUs that are difficult to inspect independently. Therefore, a process of elimination is used, where components that can be inspected individually are inspected, and if no problems are found in these components, the related ECU is identified as the problem and replaced.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>It is extremely important to ask the customer about the environment and the conditions present when the problem occurred (Customer Problem Analysis). This makes it possible to simulate the conditions and confirm the symptom. If the symptom cannot be confirmed or the DTC does not recur, the malfunctioning part may not be identified using the troubleshooting procedure, and the ECU for the related system may be replaced even though it is not defective. If this happens, the original problem will not be solved.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>In order to prevent endless expansion of troubleshooting procedures, the troubleshooting procedures are written with the assumption that multiple malfunctions do not occur simultaneously for a single problem symptom.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>To identify the malfunctioning part, troubleshooting procedures narrow down the target by separating components, ECUs and wire harnesses during the inspection. If the wire harness is identified as the cause of the problem, it is necessary to inspect not only the connections to components and ECUs but also all of the wire harness connectors between the component and the ECU.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DESCRIPTION
</span></p>
<br>
<p>System data and the Diagnostic Trouble Codes (DTCs) can be read from the Data Link Connector 3 (DLC3) of the vehicle. When the system seems to be malfunctioning, use the intelligent tester to check for a malfunction and perform repairs.
</p>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DATA LINK CONNECTOR 3 (DLC3)
</span></p>
<br>
<div class="step2">
<div class="step2Item step2HasFigure">
<div class="step2Head">a.</div>
<div class="step2Body">
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/D103500.png" alt="D103500" title="D103500">
<div class="indicateinfo">
<span class="line">
<span class="points">1.948,1.469 1.781,1.813</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">2.323,1.479 2.49,1.833</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">2.125,0.927 2.698,0.49</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.938,0.927 2.167,0.49</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.729,0.927 1.552,0.479</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.583,0.927 1.083,0.469</span>
<span class="whiteedge">true</span>
</span>
<span class="caption">
<span class="points">0.156,0.073 0.49,0.354</span>
<span class="captionSize">0.333,0.281</span>
<span class="fontsize">10</span>
<span class="captionText">*a</span>
</span>
<span class="caption">
<span class="points">0.927,0.333 1.25,0.552</span>
<span class="captionSize">0.323,0.219</span>
<span class="fontsize">10</span>
<span class="captionText">E</span>
</span>
<span class="caption">
<span class="points">1.313,0.281 1.813,0.531</span>
<span class="captionSize">0.5,0.25</span>
<span class="fontsize">10</span>
<span class="captionText">SIGE</span>
</span>
<span class="caption">
<span class="points">1.969,0.302 2.51,0.542</span>
<span class="captionSize">0.542,0.24</span>
<span class="fontsize">10</span>
<span class="captionText">CANH</span>
</span>
<span class="caption">
<span class="points">1.563,1.854 2.125,2.104</span>
<span class="captionSize">0.563,0.25</span>
<span class="fontsize">10</span>
<span class="captionText">CANL</span>
</span>
<span class="caption">
<span class="points">2.75,0.354 3.156,0.573</span>
<span class="captionSize">0.406,0.219</span>
<span class="fontsize">10</span>
<span class="captionText">SIO</span>
</span>
<span class="caption">
<span class="points">2.479,1.854 2.885,2.125</span>
<span class="captionSize">0.406,0.271</span>
<span class="fontsize">10</span>
<span class="captionText">+B</span>
</span>
</div>
</div>
<table summary="caption-table" class="half">
<colgroup>
<col style="width:20%">
<col style="width:79%">
</colgroup>
<tbody>
<tr>
<td class="alcenter">*a
</td>
<td>Front view of DLC3
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<br>
</div>
<p>The vehicle ECU uses the ISO 15765-4 communication protocol. The terminal arrangement of the DLC3 complies with SAE J1962 and matches the ISO 15765-4 format.
</p>
<table summary="">
<colgroup>
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Terminal No. (Symbol)
</th>
<th class="alcenter">Terminal Description
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">7 (SIO) - 5 (SIGE)
</td>
<td class="alcenter">Bus "+" line
</td>
<td class="alcenter">During transmission
</td>
<td class="alcenter">Pulse generation
</td>
</tr>
<tr>
<td class="alcenter">4 (E) - Body ground
</td>
<td class="alcenter">Chassis ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">5 (SIGE) - Body ground
</td>
<td class="alcenter">Signal ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">16 (+B) - Body ground
</td>
<td class="alcenter">Battery positive
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">11 to 14 V
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 14 (CANL)
</td>
<td class="alcenter">CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">54 to 69 Ω
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 4 (E)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 4 (E)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 16 (+B)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 16 (+B)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch off*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3">
<p>*: Before measuring the resistance, leave the vehicle as is for at least 1 minute and do not operate the ignition switch, any other switches, or the doors.
</p>
</dd>
</dl>
<br>
<p>If the result is not as specified, the DLC3 may have a malfunction. Repair or replace the harness and connector.
</p>
</div>
</div>
</div>
<br>
<div class="step2">
<div class="step2Item">
<div class="step2Head">b.</div>
<div class="step2Body">
<p>Connect the cable of the intelligent tester to the DLC3, turn the ignition switch on and attempt to use the tester. If the display indicates that a communication error has occurred, there is a problem either with the vehicle or with the tester.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is normal when the tester is connected to another vehicle, inspect the DLC3 of the original vehicle.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is still not possible when the tester is connected to another vehicle, the problem may be in the tester itself. Consult the Service Department listed in the tester's instruction manual.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<dl class="topic">
<dt class="topic">FOR USING INTELLIGENT TESTER</dt>
<dd class="topic"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Before using the intelligent tester, read the tester operator's manual thoroughly.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If the tester cannot communicate with the ECU controlled systems when connected to the DLC3 with the ignition switch in the on position and the tester turned on, there is a problem on the vehicle side or tester side.
</p>
</div>
</div>
<div class="list2">
<div class="list2Item">
<div class="list2Head">1.</div>
<div class="list2Body"><p>If communication is possible when the tester is connected to another vehicle, inspect the diagnosis data link line (bus (+) line), CANH and CANL lines, and the power circuits for the vehicle ECUs.
</p>
</div>
</div>
<div class="list2Item">
<div class="list2Head">2.</div>
<div class="list2Body"><p>If communication is still not possible when the tester is connected to another vehicle, the problem is probably in the tester itself. Perform the Self Test procedure outlined in the tester operator's manual.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
